#!bin/bash

pushd "$1";

n=1;
for file in ./*.in;
do
    echo "$file";
    ../myavl < "$file" > file.sol;
    diff file.sol teste"$n".out;
    ((n++));
done

popd;

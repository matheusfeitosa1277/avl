# Avl
**Enunciado adaptado da Pagina do Prof Eduardo Cunha de Almeida - https://www.inf.ufpr.br/eduardo/ensino/ci057/2020-2/trab1/trab.html**
## Objetivo
Neste exercício relativamente simples iremos implementar a inclusão e exclusão de valores de árvore AVL vista em sala de aula.
## Entrada:
A entrada deve ser feita pela entrada padrão (`stdin`). O arquivo é formado por uma sequência de linhas, onde cada linha representa uma operação com uma chave inteira chegando. A ordem das operações é aleatória. Cada operação pode ser de inclusão (i) ou remoção (r). Portanto, o primeiro campo é o tipo da operação e o segundo campo é a chave. Por exemplo, "i 10" significa a inclusão da chave 10 na AVL.
## Saída:
A saída deve ser feita pela saída padrão (`stdout`). A escrita na saída pode acontecer quando a árvore toda for criada. O arquivo será composto por uma sequência de linhas. Uma linha para cada valor lido no formato (valor, nivel). Portanto, cada linha tem 2 campos separados por vírgula. O primeiro campo é o valor lido "inorder". O segundo campo é o nivel da árvore na qual o valor se encontra de acordo com o algoritmo AVL. A saida será ordenada pelo primeiro campo e em seguida pelo segundo campo em caso de chaves duplicadas na árvore.

## Exemplo:
![Exemplo Entrada](https://www.inf.ufpr.br/msf21/resources/avl.png)

## Bugs...

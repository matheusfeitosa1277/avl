#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "avl.h"

#define LINESIZE 1024
int main(){
    char *op, *lineBuf = malloc(sizeof(char) * LINESIZE);
    int chave;

    avlNo *raiz = NULL;

    while (fgets(lineBuf, LINESIZE, stdin)){ //Leitura da Arvore
        op = strtok(lineBuf, " "); chave = atoi(strtok(NULL, "\n"));
        switch(*op){
            case 'i' : raiz = inclusao(raiz, chave) ; break;
            case 'r' : raiz = exclusao(raiz, chave); break;
        }
    }
    free(lineBuf);
    
    int aux = 0, *pAux = &aux;
    emOrdem(raiz, pAux);
    freeArvore(raiz);
    return 0;
}


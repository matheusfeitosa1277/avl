CFLAGS = -Wall
PROGNAME = myavl

all: main.o avl.o avl.h
	gcc main.o avl.o -o $(PROGNAME) $(CFLAGS) 
main.o: main.c avl.h
	gcc -c main.c $(CFLAGS)
avl.o: avl.c avl.h
	gcc -c avl.c $(CFLAGS)
clean:
	rm -f *.o
purge: clean
	rm -f $(PROGNAME)

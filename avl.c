#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "avl.h"

#define MAX(a, b) ((a) > (b) ? (a) : (b))

/* Struct Avl - Libera Memoria */
avlNo *criaNo(int chave){
    avlNo *no = malloc(sizeof(avlNo));

    no->chave = chave;
    no->esq = NULL; no->dir = NULL;
    no->alt = 1;

    return no;
}

avlNo *freeNo(avlNo *no){
    free(no);

    return NULL;
}

void freeArvore(avlNo *raiz){ //Mesma ideia da Travessia, so que liberando memoria ao inves de imprimir
    if (raiz) { 
        freeArvore(raiz->esq);
        freeArvore(raiz->dir);
        freeNo(raiz);
    }
}

/* Inclusao na Folha */

/* Propriedades da AVL */
int altura(avlNo *no){
    if (!no) return 0;
    return no->alt;
}

int balanceamentos(avlNo *no){
    if (!no) return 0;

    return altura(no->esq) - altura(no->dir);
}
/* . . . */

avlNo *inclusao(avlNo *no, int chave){
    if (!no) return criaNo(chave); //Arvore Vazia

    if (no->chave > chave)
        no->esq = inclusao(no->esq, chave); //Insere Esq
    else
        no->dir = inclusao(no->dir, chave); //Insere Dir

    /* Propriedades da AVL */
    no->alt = 1 + MAX(altura(no->esq), altura(no->dir));
    int balanceamento = balanceamentos(no);

    if (balanceamento > 1){
       if (chave > no->esq->chave) no->esq = rotEsq(no->esq);
       return rotDir(no);
    }

    else if (balanceamento < -1){
        if (chave < no->dir->chave) no->dir = rotDir(no->dir);
        return rotEsq(no);
    }
    /* . . . */

    return no;
}

/* Busca Rec - Cormen */
avlNo *busca(avlNo *raiz, int cBusca){
    if (!raiz || raiz->chave == cBusca) return raiz; //(FolhaNULL ? NULL : Nodo)

    if (raiz->chave > cBusca) return busca(raiz->esq, cBusca); //Procura a Esquerda

    return busca(raiz->dir, cBusca); //Procura a Direita
}

/* Exclusao */
avlNo *exclusao(avlNo *raiz, int cBusca){
    if (!raiz) return raiz;

    if (raiz->chave > cBusca) //Procura a Esquerda
        raiz->esq = exclusao(raiz->esq, cBusca);
    else if (raiz->chave < cBusca) //Procura a Direita
        raiz->dir = exclusao(raiz->dir, cBusca);

    /* Mesma Chave - No a ser deletado - Cormem */
    else {
        avlNo *aux;

        if (!(raiz->esq || raiz->dir)){ //Nenhum Filho
            aux = raiz;
            raiz = NULL;
            freeNo(aux);
        }

        /* Por algumo motivo o XOR nao funciona, mesmo com o cast pra uintptr_t... */
        else if ( (raiz->esq != NULL) ^ (raiz->dir != NULL)){ //Um Filho
            aux = raiz->esq ? raiz->esq : raiz->dir; //Descobre o !NULL
            *raiz = *aux;
            freeNo(aux);
        }

        else { //Dois Filhos
            aux = tMax(raiz->esq);
            raiz->chave = aux->chave;
            raiz->esq = exclusao(raiz->esq, aux->chave);
        }
    }

    if (!raiz) return raiz; //Arvore tinha apenas 1 no, agora esta vazia

    /* Auto Balanceamento */
    raiz->alt = 1 + MAX(altura(raiz->esq), altura(raiz->dir));
    int balanceamento = balanceamentos(raiz);

    if (balanceamento > 1){
        balanceamento  = balanceamentos(raiz->esq);

        if (balanceamento < 0) raiz->esq = rotEsq(raiz->esq); //Esq-Dir
        return rotDir(raiz); //Esq-Esq 
    }

    else if (balanceamento < -1){
        balanceamento = balanceamentos(raiz->dir);

        if (balanceamento > 0) raiz->dir = rotDir(raiz->dir); //Dir-Esq
        return rotEsq(raiz); //Dir-Dir
    }
    /* . . . */

    return raiz;
}

/* Rotacao */
avlNo *rotDir(avlNo *raiz){
    avlNo *pivo;

    pivo = raiz->esq;
    raiz->esq = pivo->dir;
    pivo->dir = raiz;

    /* . . . */
    raiz->alt = MAX(altura(raiz->esq), altura(raiz->dir))+1;
    pivo->alt = MAX(altura(pivo->esq), altura(pivo->dir))+1;
    /* . . . */

    return pivo;
}

avlNo *rotEsq(avlNo *raiz){
    avlNo *pivo;

    pivo = raiz->dir;
    raiz->dir = pivo->esq;
    pivo->esq = raiz;

    /* . . . */
    raiz->alt = MAX(altura(raiz->esq), altura(raiz->dir))+1;
    pivo->alt = MAX(altura(pivo->esq), altura(pivo->dir))+1;
    /* . . . */

    return pivo;
}

/* Travessia - Cormem */
void emOrdem(avlNo *raiz, int *alt){
    if (raiz) {
        ++*alt; emOrdem(raiz->esq, alt); --*alt;
        printf("%d,%d\n", raiz->chave, *alt);
        ++*alt; emOrdem(raiz->dir, alt); --*alt;
    }
}

void preOrdem(avlNo *raiz){
    if (raiz) {
        printf("(%2d) ", raiz->chave);
        preOrdem(raiz->esq);
        preOrdem(raiz->dir);
    }
}

/* --------- */
/* Outros... */
/* --------- */

/* Minimo/Max - Cormem */
avlNo *tMin(avlNo *raiz){
   if (!raiz->esq) return raiz; 

   return tMin(raiz->esq);
}

avlNo *tMax(avlNo *raiz){
    if (!raiz->dir) return raiz;

    return tMax(raiz->dir);
}


#ifndef AVL_H
#define AVL_H

/* -------------------------------------- */
/* AVL - Adelson-Velsky and Landis - Tree */
/* -------------------------------------- */

typedef struct avlNo { 
    int chave; //id de busca
    struct avlNo *esq, *dir;

    int alt; //Alt do No, comecando pela folha em 1

} avlNo;

avlNo *criaNo(int chave);

avlNo *freeNo(avlNo *no);

void freeArvore(avlNo *raiz);

/* Inclusao */
avlNo *inclusao(avlNo *no, int chave);

/* Busca */
avlNo *busca(avlNo *raiz, int chave);

/* Exclusao */
avlNo *exclusao(avlNo *no, int chave);

/* Rotacao */
avlNo *rotEsq(avlNo *raiz);

avlNo *rotDir(avlNo *raiz);

/* Travessia */
void emOrdem(avlNo *raiz, int *alt);

void preOrdem(avlNo *raiz);

/* Outros... */
avlNo *tMin(avlNo *raiz);

avlNo *tMax(avlNo *raiz);

#endif

